<?php
session_start();
include '../koneksi.php';
if(!isset($_SESSION['username'])){
    echo "<script type=text/javascript>alert('Anda Belum Login');
    window.location.href='../index.php';
    </script>";
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Inventaris</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Admin_Inventaris
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Administrator<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                
                               
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    
                                    <div class="pull-right">
                                        <a href="../logout.php" class="btn btn-default btn-flat">Logout</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
       <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="../img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, Admin</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
       <ul class="sidebar-menu">
                        <li>
                            <a href="../admin/index.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                       </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-tasks"></i> <span>Inventaris</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="dt_barang.php"><i class="fa fa-angle-double-right"></i> Data Barang</a></li>
                                <li><a href="dt_jenis.php"><i class="fa fa-angle-double-right"></i> Data Jenis Barang</a></li>
                                 <li><a href="dt_ruangan.php"><i class="fa fa-angle-double-right"></i> Data Ruangan</a></li>
                            </ul>
                        </li>
                       <li class="treeview">
                            <a href="#">
                                <i class="fa fa-random"></i> <span>Transaksi</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="peminjaman.php"><i class="fa fa-angle-double-right"></i>
                                Peminjaman</a></li>
                                <li><a href="pengembalian.php"><i class="fa fa-angle-double-right"></i>
                                Pengembalian</a></li>
                            </ul>
                            
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-user"></i> <span>Petugas</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="dt_petugas.php"><i class="fa fa-angle-double-right"></i> Data Petugas</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-users"></i> <span>Pegawai</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="dt_pegawai.php"><i class="fa fa-angle-double-right"></i> Data Pegawai</a></li>
                            </ul>
                       
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Laporan</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="laporan_barang.php"><i class="fa fa-angle-double-right"></i>Laporan Barang</a></li>
                                
                            </ul>
                        </li>
                     <li class="treeview">
                                <li><a href="backup.php"><i class="fa fa-download"></i>Backup</a></li>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Detail Peminjaman
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Peminjaman</li>
                    </ol>
                </section>
<div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Peminjaman</h3>         
                                                               
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                <div class="panel-body">
                             <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" ><i class="fa fa-plus" aria-hidden="true" style="margin-bottom: 10px;" style="color: white;"> Pinjam</i></a></button>
<br>
<br>
                                <table id="tester" class="table table-striped table-bordered table-hover">
                                    <div class="table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah Pinjam</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                           <?php
                                           include '../koneksi.php';
                                           $no =1;
                                           $id=$_GET['id_peminjaman'];
                                             $select = mysqli_query($koneksi,"SELECT * from inventaris INNER JOIN detail_pinjam ON inventaris.id_inventaris=detail_pinjam.id_inventaris where detail_pinjam.id_peminjaman='$id' ORDER BY inventaris.id_inventaris desc");
                                         
                                          

                                           while($r = mysqli_fetch_array($select)){
                                              ?>

                                              <tr>
                                                  <th scope="row"><?php echo $no++;?></th>
                                                  <td><?php echo $r['nama']; ?></td>
                                                  <td><?php echo $r['jumlah']; ?>buah</td>
                                              </tr>
                                              <?php 
                                          }
                                          ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>




                       



                    </div> <!-- container -->

                </div> <!-- content -->

              

            </div>

 

<div id="myModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">peminjam</h4>
        </div>
        <div class="modal-body">
        <form method="POST" action="proses_detail_pinjam.php">
        <div class="form-group">
                 <div class="form-group">
          <label>Jumlah</label>  
           <input type="text"  name="jumlah" class="form-control" 
              onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
            onkeyup   ="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required>
          <input type="hidden" name="id_peminjaman"  value="<?php echo $_GET['id_peminjaman']?>" class="from-control">
        </div> 
        <label>Nama Barang</label>
          <select class="form-control"  name="id_inventaris"  required="">
              <option value="">--- Silahkan Cari ---</option>

              <?php
              include_once "../koneksi.php";
              $tampil=mysqli_query($koneksi,"SELECT * FROM inventaris jumlah ORDER BY id_inventaris desc");
              while($r=mysqli_fetch_array($tampil)){
                ?>
                <option value="<?php echo $r['id_inventaris']?>"> <?php echo $r['nama'] ?> (<?php echo $r['jumlah'] ?>)</option>
                <?php
            }
            ?>

        </select>
        </div>

        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <input type="submit" name="pbarang" Value="Tambah" class="btn btn-primary">
        </div>
        </form>
      </div>
    </div>
  </div>
                                    

        <!-- add new calendar event modal -->


       
        <!-- jQuery 2.0.2 -->
        <script src="../js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="../js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
    </body>
</html>
<script type="text/javascript" language=JavaScript>
function inputDigitsOnly(e) {
 var chrTyped, chrCode=0, evt=e?e:event;
 if (evt.charCode!=null)     chrCode = evt.charCode;
 else if (evt.which!=null)   chrCode = evt.which;
 else if (evt.keyCode!=null) chrCode = evt.keyCode;

 if (chrCode==0) chrTyped = 'SPECIAL KEY';
 else chrTyped = String.fromCharCode(chrCode);

 //[test only:] display chrTyped on the status bar 
 self.status='inputDigitsOnly: chrTyped = '+chrTyped;

 //Digits, special keys & backspace [\b] work as usual:
 if (chrTyped.match(/\d|[\b]|SPECIAL/)) return true;
 if (evt.altKey || evt.ctrlKey || chrCode<28) return true;

 //Any other input? Prevent the default response:
 if (evt.preventDefault) evt.preventDefault();
 evt.returnValue=false;
 return false;
}

function addEventHandler(elem,eventType,handler) {
 if (elem.addEventListener) elem.addEventListener (eventType,handler,false);
 else if (elem.attachEvent) elem.attachEvent ('on'+eventType,handler); 
 else return 0;
 return 1;
}

// onload: Call the init() function to add event handlers!
function init() {
 addEventHandler(self.document.f2.elements[0],'keypress',inputDigitsOnly);
 addEventHandler(self.document.f2.elements[1],'keypress',inputDigitsOnly);
}

</script>