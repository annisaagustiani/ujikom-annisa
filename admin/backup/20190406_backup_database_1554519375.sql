DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `jumlah` varchar(30) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_peminjaman` (`id_peminjaman`),
  KEY `id_peminjaman_2` (`id_peminjaman`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("5","2","1","10");
INSERT INTO detail_pinjam VALUES("6","2","1","0");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` text NOT NULL,
  `spesifikasi` text NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `sumber` text NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  UNIQUE KEY `id_inventaris_2` (`id_inventaris`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_inventaris` (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("1","Laptop","Bagus","Lenovo Intel Core i3","dipinjam","0","1","2019-02-28","1","V0001","2","PT. Prima");
INSERT INTO inventaris VALUES("2","Laptop","Bagus"," Lenovo amd A4","dipinjam","9","1","2019-03-01","2","V0002","2","PT. Prima");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("1","Elektronik","J0001","dipinjam");
INSERT INTO jenis VALUES("2","Non Elektronik","J0002","dipinjam");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","peminjaman");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` int(11) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("2","salsa","2311","laladon");
INSERT INTO pegawai VALUES("5","fani","12334","Ciomas");



DROP TABLE peminjam;

;




DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  `status_peminjam` enum('dipinjam','dikembalikan') NOT NULL DEFAULT 'dipinjam',
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_pegawai` (`id_pegawai`),
  KEY `id_pegawai_2` (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("11","2019-04-05","","dipinjam","2");
INSERT INTO peminjaman VALUES("12","2019-04-05","","dipinjam","5");
INSERT INTO peminjaman VALUES("13","2019-04-05","","dipinjam","2");
INSERT INTO peminjaman VALUES("14","2019-04-05","","dipinjam","5");
INSERT INTO peminjaman VALUES("15","2019-04-05","","dipinjam","5");
INSERT INTO peminjaman VALUES("16","2019-04-05","","dipinjam","5");
INSERT INTO peminjaman VALUES("17","2019-04-05","","dipinjam","5");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  `baned` enum('N','Y','') NOT NULL,
  `logintime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","administrator","admin","annisaagustiani15@gmail.com","admin","1","N","0");
INSERT INTO petugas VALUES("2","operator","operator","imanelvanhaz@gmail.com","operator","2","N","0");
INSERT INTO petugas VALUES("7","peminjaman","pinjam","desya@gmail.com","desya","3","N","0");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","LAB RPL 1","R0001","dipinjam");
INSERT INTO ruang VALUES("2","LAB RPL 2","R0002","dipinjam");
INSERT INTO ruang VALUES("3","Studio ANM","R0003","dipinjam");



