DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_peminjam` int(11) NOT NULL,
  `status` enum('dipinjam','dikembalikan','','') NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_inventaris` (`id_inventaris`),
  KEY `id_peminjam` (`id_peminjam`),
  KEY `id_peminjam_2` (`id_peminjam`),
  KEY `id_peminjam_3` (`id_peminjam`),
  KEY `id_peminjam_4` (`id_peminjam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `kondisi` text NOT NULL,
  `spesifikasi` text NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tgl_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(30) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `sumber` text NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_petugas` (`id_petugas`),
  KEY `id_ruang` (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("29","Laptop","baik","Lenovo Intel Core i3","baik","1","1","2019-02-27","2","V0001","5","Sekolah");
INSERT INTO inventaris VALUES("30","spidol","baik","snowman","baru","20","2","2019-02-27","2","V0002","8","supplier");
INSERT INTO inventaris VALUES("31","meja","baik","Mebeler","baik","2","2","2019-02-28","1","V0003","8","supplier");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` varchar(10) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("1","Elektronik","B0001","baik");
INSERT INTO jenis VALUES("2","Non Elektronik","B0002","dipinjam");
INSERT INTO jenis VALUES("6","BLABLA","B0003","baru");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","peminjam");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(30) NOT NULL,
  `nip` int(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("2","agista","170920","gunungbatu");
INSERT INTO pegawai VALUES("3","annisa","170921","gunungbatu");
INSERT INTO pegawai VALUES("4","nava","98209739","laladon");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(20) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_pegawai` (`id_pegawai`),
  KEY `id_pegawai_2` (`id_pegawai`),
  KEY `id_pegawai_3` (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("1","2019-01-07","2019-01-07","vghc","1");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(15) NOT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("5","admin","annisaagustiani15@gmail.com","aksAkd4f","administrator","1");
INSERT INTO petugas VALUES("7","operator","annisaagustiani15@gmail.com","operator","annisa","2");
INSERT INTO petugas VALUES("8","peminjam","annisaagustiani15@gmail.com","peminjam","agista","3");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(30) NOT NULL,
  `kode_ruang` varchar(10) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Lab RPL 1","R0001","baik");
INSERT INTO ruang VALUES("2","Lab RPL 2","R0002","baik");



