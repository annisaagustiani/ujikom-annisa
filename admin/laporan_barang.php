<?php
session_start();
include '../koneksi.php';
if(!isset($_SESSION['username'])){
    echo "<script type=text/javascript>alert('Anda Belum Login');
    window.location.href='../index.php';
    </script>";
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Inventaris</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Admin_Inventaris
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Administrator<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                   
                                    <div class="pull-right">
                                        <a href="../logout.php" class="btn btn-default btn-flat">Logout</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
       <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="../img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, admin</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
                        <li>
                            <a href="../admin/index.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                       </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-tasks"></i> <span>Inventaris</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="dt_barang.php"><i class="fa fa-angle-double-right"></i> Data Barang</a></li>
                                <li><a href="dt_jenis.php"><i class="fa fa-angle-double-right"></i> Data Jenis Barang</a></li>
                                 <li><a href="dt_ruangan.php"><i class="fa fa-angle-double-right"></i> Data Ruangan</a></li>
                            </ul>
                        </li>
                       <li class="treeview">
                            <a href="#">
                                <i class="fa fa-random"></i> <span>Transaksi</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="peminjaman.php"><i class="fa fa-angle-double-right"></i>
                                Peminjaman</a></li>
                                <li><a href="pengembalian.php"><i class="fa fa-angle-double-right"></i>
                                Pengembalian</a></li>
                            
                            </ul>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-user"></i> <span>Petugas</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="dt_petugas.php"><i class="fa fa-angle-double-right"></i> Data Petugas</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-users"></i> <span>Pegawai</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="dt_pegawai.php"><i class="fa fa-angle-double-right"></i> Data Pegawai</a></li>
                            </ul>
                       
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Laporan</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                           <ul class="treeview-menu">
                                <li><a href="laporan_barang.php"><i class="fa fa-angle-double-right"></i>Laporan Barang</a></li>
                               
                            </ul>
                        </li>
                     <li class="treeview">
                                <li><a href="backup.php"><i class="fa fa-download"></i>Backup</a></li>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Laporan 
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                      <div class="box">
                                <div class="box-header">
                                <div class="card-box table-responsive">
                                        <h3 class="box-title">Laporan Barang</h3>
                                    <table id="tester" class="table table-striped table-bordered table-hover">
                                    <div class="table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">

                                    
                                    
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Kondisi</th>
                                            <th>Keterangan</th>
                                            <th>Jumlah</th>
                                            <th>Jenis</th>
                                            <th>Tanggal Registrasi</th>
                                            <th>Ruang</th>
                                            <th>Kode Inventaris</th>
                                            <th>Petugas</th>
                                            <th>Sumber</th>
                                            
                                        </tr>
                                        </thead>


                                        <tbody>
                                         <?php
                                        include '../koneksi.php';
                                        $no =1;
                                         
                                        $data = mysqli_query($koneksi,"SELECT * FROM inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis  INNER JOIN ruang ON inventaris.id_ruang=ruang.id_ruang INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas  order by id_inventaris desc ");
                                       
                                        while($r = mysqli_fetch_array($data)){
                                        ?>
                                        <tr>
                                            <td><?php echo $no++;?></td>
                                            <td><?php echo $r['nama']; ?></td>
                                            <td><?php echo $r['kondisi']; ?></td>
                                            <td><?php echo $r['keterangan']; ?></td>
                                            <td><?php echo $r['jumlah']; ?></td>
                                            <td><?php echo $r['nama_jenis']; ?></td>
                                            <td><?php echo $r['tanggal_register']; ?></td>
                                            <td><?php echo $r['nama_ruang']; ?></td>
                                            <td><?php echo $r['kode_inventaris']; ?></td>
                                            <td><?php echo $r['nama_petugas']; ?></td>
                                            <td><?php echo $r['sumber']; ?></td>
                                            

                                        </tr>
                                       <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <button type="button"  class="btn btn-danger" data-toggle="modal" data-target="#export" >
                                    <i class="fa fa-print" aria-hidden="true" style="margin-bottom: 10px;" style="color: white;"> Cetak PDF</i></a></button>
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#excel" >
                                    <i class="fa fa-print" aria-hidden="true" style="margin-bottom: 10px;" style="color: white;"> Export Ke Excel</i></a></button>
                                   
                                    
                                </div>
                            </div>
                        </div>




                       



                    </div> <!-- container -->

                </div> <!-- content -->

               

            </div>


<div id="export" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Rekap Inventaris </h4>
</div>
<div class="modal-body">
<form action="cetak_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-inverse btn-sm" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="ctk_inventaris.php" target="_blank" class="btn btn-inverse btn-sm" >Cetak Semua</a>
</div>
</div>
</div>
</div>

<!-- batas woi -->

<div id="excel" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Rekap Inventaris </h4>
</div>
<div class="modal-body">
<form action="export_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-inverse btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="laporan_inventaris_excel.php" target="_blank" class="btn btn-inverse btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
               

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="../js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="../js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>   

    </body>
</html>