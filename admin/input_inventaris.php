<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Inventaris</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="../admin/index.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Admin_Inventaris
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                       
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Administrator<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                
                               
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    
                                    <div class="pull-right">
                                        <a href="../logout.php" class="btn btn-default btn-flat">Logout</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
         <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="../img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, Admin</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
              <ul class="sidebar-menu">
                        <li>
                            <a href="../admin/index.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                       </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-tasks"></i> <span>Inventaris</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="dt_barang.php"><i class="fa fa-angle-double-right"></i> Data Barang</a></li>
                                <li><a href="dt_jenis.php"><i class="fa fa-angle-double-right"></i> Data Jenis Barang</a></li>
                                 <li><a href="dt_ruangan.php"><i class="fa fa-angle-double-right"></i> Data Ruangan</a></li>
                            </ul>
                        </li>
                       <li class="treeview">
                            <a href="#">
                                <i class="fa fa-random"></i> <span>Transaksi</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="peminjaman.php"><i class="fa fa-angle-double-right"></i>
                                Peminjaman</a></li>
                                <li><a href="pengembalian.php"><i class="fa fa-angle-double-right"></i>
                                Pengembalian</a></li>
                            
                            </ul>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-user"></i> <span>Petugas</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="dt_petugas.php"><i class="fa fa-angle-double-right"></i> Data Petugas</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-users"></i> <span>Pegawai</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="dt_pegawai.php"><i class="fa fa-angle-double-right"></i> Data Pegawai</a></li>
                            </ul>
                       
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Laporan</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                           <ul class="treeview-menu">
                                <li><a href="laporan_barang.php"><i class="fa fa-angle-double-right"></i>Laporan Barang</a></li>
                                
                            </ul>
                        </li>
                     <li class="treeview">
                                <li><a href="backup.php"><i class="fa fa-download"></i>Backup</a></li>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
               
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                    <form action="simpan_inventaris.php" method="post" enctype="multipart/form-data" name="form1" id="form1">  

                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Form</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama Barang</label>
                                            <input type="text" class="form-control" name="nama" id="nama" required="" placeholder="nama barang">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Kondisi</label>
                                            <input type="text" class="form-control" name="kondisi" id="kondisi" required="" placeholder="kondisi">
                                        </div>
                                         <div class="form-group">
                                            <label for="exampleInputPassword1">Spesifikasi</label>
                                            <input type="text" class="form-control" name="spesifikasi" id="spesifikasi" required="" placeholder="spesifikasi">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Keterangan</label>
                                            <input type="text" class="form-control" name="keterangan" id="keterangan" required="" placeholder="keterangan">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Jumlah</label>
                                            <input type="jumlah" class="form-control" name="jumlah" id="jumlah" required="" placeholder="jumlah">
                                        </div>

 <?php
  include "../koneksi.php";
  $result = mysqli_query($koneksi,"select * from jenis order by id_jenis asc ");
  $jsArray = "var id_jenis = new Array();\n";
  ?>

    <label>Jenis :</label> <select class="form-control" name="id_jenis" onchange="changeValue(this.value)">
  <option selected="selected">..........Pilih Jenis..........
  <?php 
  while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
  }
  ?>
</option>
</select> 
<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input type="date" name="tanggal_register" class="form-control" value="<?php 
    $tanggal = Date('Y-m-d');
    echo $tanggal;
    ?>"  readonly>
        </div> 
 
 <?php
  include "../koneksi.php";
  $result = mysqli_query($koneksi,"select * from ruang order by id_ruang asc ");
  $jsArray = "var id_ruang = new Array();\n";
  ?>

    <label>Ruang</label> <select class="form-control" name="id_ruang" onchange="changeValue(this.value)">
  <option selected="selected">..........Pilih Ruang..........
  <?php 
  while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
  }
  ?>
</option>
</select> 
<div class="form-group">
    <label for="exampleInputPassword1">Kode Inventaris</label>
    <div class="col-10">
    <?php
    $koneksi = mysqli_connect("localhost","root","","ujikom");
    $cari_kd = mysqli_query($koneksi,"select max(kode_inventaris) as kode from inventaris");
    //besar atau kode yang baru masuk
    $tm_cari=mysqli_fetch_array($cari_kd);
    $kode=substr($tm_cari['kode'],1,4);
    $tambah=$kode+1;
    if($tambah<10){
        $kode_inventaris="V000".$tambah;
    }else{
        $kode_inventaris="V00".$tambah;
    }
    ?>

    <input type="text" value="<?php echo $kode_inventaris;?>" class="form-control" name="kode_inventaris" id="kode_inventaris" placeholder="kode inventaris" readonly>
    </div>

<?php
  include "../koneksi.php";
  $result = mysqli_query($koneksi,"select * from petugas order by id_petugas asc ");
  $jsArray = "var id_petugas = new Array();\n";
  ?>

    <label>ID Petugas :</label> <select class="form-control" name="id_petugas" onchange="changeValue(this.value)">
  <option selected="selected">..........Pilih ID Petugas..........
  <?php 
  while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
  }
  ?>
</option>
</select>
                                            <div class="form-group">
                                            <label for="exampleInputPassword1">Sumber</label>
                                            <input type="sumber" class="form-control" name="sumber" id="sumber" required="" placeholder="sumber">
                                        </div> 
</form>

                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="sumbit" name="simpan" id="simpan" class="btn btn-primary">Simpan</button>
                                        <a class="btn btn-danger" href="dt_barang.php" type="cancel">Batal</a>
                                    </div>
                                </form>
                            </div><!-- /.box -->

                           
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!--/.col (right) -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="../js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="../js/AdminLTE/app.js" type="text/javascript"></script>
    </body>
</html>