-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2019 at 02:59 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ujikom`
--
CREATE DATABASE IF NOT EXISTS `ujikom` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ujikom`;

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE IF NOT EXISTS `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `jumlah` varchar(30) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_peminjaman` (`id_peminjaman`),
  KEY `id_peminjaman_2` (`id_peminjaman`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `jumlah`, `id_peminjaman`) VALUES
(20, 1, '2', 27),
(21, 3, '2', 28);

--
-- Triggers `detail_pinjam`
--
DROP TRIGGER IF EXISTS `kembali`;
DELIMITER //
CREATE TRIGGER `kembali` BEFORE UPDATE ON `detail_pinjam`
 FOR EACH ROW UPDATE inventaris set jumlah=jumlah+NEW.jumlah where id_inventaris=new.id_inventaris
//
DELIMITER ;
DROP TRIGGER IF EXISTS `pengurangan_stok`;
DELIMITER //
CREATE TRIGGER `pengurangan_stok` AFTER INSERT ON `detail_pinjam`
 FOR EACH ROW UPDATE inventaris SET jumlah=jumlah-NEW.jumlah where id_inventaris=new.id_inventaris
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE IF NOT EXISTS `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` text NOT NULL,
  `spesifikasi` text NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `sumber` text NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  UNIQUE KEY `id_inventaris_2` (`id_inventaris`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_inventaris` (`id_inventaris`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `spesifikasi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`, `sumber`) VALUES
(1, 'Laptop Acer', 'Baik', 'Intel Core i3', 'Ada', 70, 1, '2019-02-28', 1, 'V0001', 2, 'PT. Prima'),
(2, 'Laptop Lenovo', 'Baik', 'Intel Core i3', 'Ada', 61, 1, '2019-03-01', 2, 'V0002', 2, 'PT. Prima'),
(3, 'spidol', 'Baik', 'snowman', 'Ada', 48, 2, '2019-04-06', 2, 'V0003', 1, 'Sekolah');

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE IF NOT EXISTS `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'Elektronik', 'J0001', 'Ada'),
(2, 'Non Elektronik', 'J0002', 'Ada');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'operator'),
(3, 'peminjaman');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` int(11) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(2, 'salsa', 2311, 'laladon'),
(5, 'fani', 12334, 'Ciomas'),
(6, 'annisa', 171724, 'gunungbatu');

-- --------------------------------------------------------

--
-- Table structure for table `peminjam`
--
-- in use(#1356 - View 'ujikom.peminjam' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them)
-- Error reading data: (#1356 - View 'ujikom.peminjam' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them)

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE IF NOT EXISTS `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  `status_peminjam` enum('dipinjam','dikembalikan') NOT NULL DEFAULT 'dipinjam',
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_pegawai` (`id_pegawai`),
  KEY `id_pegawai_2` (`id_pegawai`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjam`, `id_pegawai`) VALUES
(27, '2019-04-06', '2019-04-06', 'dikembalikan', 6),
(28, '2019-04-06', NULL, 'dipinjam', 5);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE IF NOT EXISTS `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  `baned` enum('N','Y','') NOT NULL,
  `logintime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `email`, `nama_petugas`, `id_level`, `baned`, `logintime`) VALUES
(1, 'administrator', 'admin', 'annisaagustiani15@gmail.com', 'admin', 1, 'N', 0),
(2, 'operator', 'operator', 'imanelvanhaz@gmail.com', 'operator', 2, 'N', 0),
(7, 'peminjaman', 'pinjam', 'desya@gmail.com', 'desya', 3, 'N', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE IF NOT EXISTS `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(1, 'LAB RPL 1', 'R0001', 'Ada'),
(2, 'LAB RPL 2', 'R0002', 'Ada'),
(3, 'Studio ANM', 'R0003', 'Ada');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
